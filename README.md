This file contains a list to install Psychopy on Python 3.7 (64 bits).
Usage:
download PsychoPyDependenciesPython3.7.txt
open a command prompt (with administrator rights)
cd to the folder containing PsychoPyDependenciesPython3.7.txt
pip install -r PsychoPyDependenciesPython3.7.txt
Spyder is installed and can be started by typing 'spyder'.